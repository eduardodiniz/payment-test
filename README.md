## SETUP
1- Cria imagem do projetos e sobe container do banco de dados
docker-compose up -d 
2- Cria banco de dados e roda migrações
docker run -it --network container:postgres_payment_api -v ~/Documents/workspace/moip/api:/api payment_api bundle exec hanami db prepare
3- Sobe container da api
docker-compose up -d payment_api

A API já está pronta para receber a criação de pagamentos na URL: localhost:4567/payment/

##Considerações

A solução apesar de não completa, foi construída com intuito de estar preparada para uma possível evolução. A arquitetura foi baseada em 'Clean Architecture' ( Hexagonal Architecture) e os conceitos fundamentados em DDD.
Domain contém as regras de negócio, UseCases fazem orquestração e Infra são serviços que conversam com o mundo externo.
TDD foi utilizado para guiar o Design.

A implementação foi baseada no seguinte desenho de solução:
Payment é um bounded context que poderia ser isolado em um módulo ou em um micro-serviço.
Nesse contexto, Boleto e CreditCard pertencem ao aggregate root Payment.
O conceito de CreditCard provavelmente existiria também em um outro contexto como Users, por exemplo, onde todas as informações do cartão seriam armazenadas. Nesse contexto a única informação persistida foi o número de cartão. Outro contexto poderia ser informado da existência de um pagamento com um novo cartão e salvar as informações através de Domain Events, por exemplo.

Um conceito chamado 'FundingInstrument' foi criado na tentativa de elucidar um Strategy(Design pattern) e transparecer que Boleto e CreditCard correspondem à um 'tipo de pagamento'. No entanto, por falta de profundidade da solução, não pude identificar os comportamentos que pertecem a esse conceito. O módulo 'FundingInstrument' que está incluído na classe Boleto deveria existir também na classe CreditCard numa eventual existência desses comportamentos compartilhados. O módulo permanece lá mesmo sem utilidade real de forma proposital para exemplificar a evolução da ideia.


##Algumas observações e funcionalidades pensadas mas não implementadas
- No domínio as classes assumem que existe uma camada de validação prévia e por isso não existem verificações para tipos de pagamentos ou status não conhecidos, por exemplo.

- Alguns retornos foram forçados nos serviços de infra para tornar possível testar situações na API, como um pagamento não aprovado em um cartão de crédito. As classes FakeService's não existiriam numa situação real. Os external_services seriam gems ou outros serviços de infra criados de acordo com a necessidade.

- As classes de entidades e repositórios estão sem módulos por conta de uma limitação do framework Hanami

- Boletos estão com vencimento hard coded. Deveria existir algum tipo de configuração para boletos

- O método liquidate_debt verifica boletos pagos em atraso pois outros contextos provavelmente já teriam sido notificados de que o pagamento não foi feito. Como contexto de liberação de estoque ou cancelamento de compras por exemplo.

- FundingInstrumentGateway centraliza lógica dos tipos de pagamento no domínio. Um novo método deveria ser criado nessa classe para recupar o status do pagamento no endpoint de checagem.

- Na solução implementada, existirão registros orfãos caso ocorram erros na criação dos fundingInstruments

- As tratativas de erros não estão no formato ideal em função da priorização de outras funcionalidades.
