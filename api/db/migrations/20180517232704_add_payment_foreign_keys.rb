Hanami::Model.migration do
  change do
    alter_table :boletos do
      add_foreign_key [:payment_id], :payments
    end

    alter_table :credit_cards do
      add_foreign_key [:payment_id], :payments
    end
  end
end
