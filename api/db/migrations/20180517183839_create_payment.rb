Hanami::Model.migration do
  change do
    create_table :payments do
      primary_key :id
      column :client_id, Integer, null: false
      column :amount, Float, null: false
      column :type, String, null: false
      column :buyer_cpf, String, null: false
      column :buyer_name, String, null: false
      column :buyer_email, String, null: false
      column :created_at, DateTime
    end
  end
end
