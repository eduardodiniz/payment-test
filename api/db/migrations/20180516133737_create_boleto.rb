Hanami::Model.migration do
  change do
    create_table :boletos do
      primary_key :id
      column :number, String, null: false
      column :due_date, Date, null: false
      column :status, String, default: 'pending', null: false
      column :created_at, DateTime
      column :payment_id, Integer, null: false
    end
  end
end
