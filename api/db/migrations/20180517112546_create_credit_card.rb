Hanami::Model.migration do
  change do
    create_table :credit_cards do
      primary_key :id
      column :number, String, null: false
      column :status, String, null: false
      column :payment_id, Integer, null: false
      column :created_at, DateTime
    end
  end
end
