require 'hanami/model'
require 'domain/payment/funding_instrument'
require 'date'

class Boleto < Hanami::Entity
  include Domain::Payment::FundingInstrument

  def liquidate_debt
    fail Exception.new('Overdue Boleto') if overdue?

    true
  end

  private

  def overdue?
    DateTime.now > DateTime.strptime(self.due_date, '%Y-%m-%d')
  end
end
