require 'date'
require 'infra/boleto_service'
require 'domain/payment/boleto_repository'

module Domain
  module Payment
    class BoletoService
      def initialize(overrides = {})
        @boleto_repository = overrides.fetch(:boleto_repository) { BoletoRepository.new }
        @infra_service = overrides.fetch(:infra_service) { Infra::BoletoService.new }
      end

      def generate_boleto(payment_id, cpf, amount)
        @infra_service.generate_boleto(cpf: cpf, amount: amount).tap do |response|
          return false unless response.error.nil?
          return @boleto_repository.create(number: response.number, due_date: Date.today.to_s, payment_id: payment_id)
        end
      end
    end
  end
end
