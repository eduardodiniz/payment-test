require 'domain/payment/boleto_service'
require 'domain/payment/credit_card_service'

module Domain
  module Payment
    class FundingInstrumentGateway
      def initialize(overrides = {})
        @boleto_service = overrides.fetch(:boleto_service) { Domain::Payment::BoletoService.new }
        @credit_card_service = overrides.fetch(:credit_card_service) { Domain::Payment::CreditCardService.new }
      end
      
      def create(payment:, credit_card_attributes: nil)
        return @boleto_service.generate_boleto(payment.id, payment.buyer_cpf, payment.amount) if payment.type == 'boleto'

        @credit_card_service.validate_payment(payment.id, payment.buyer_cpf, payment.amount, credit_card_attributes)
      end
    end
  end
end

