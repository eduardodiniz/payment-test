require 'infra/credit_card_service'
require 'domain/payment/credit_card_repository'

module Domain
  module Payment
    class CreditCardService
      def initialize(overrides = {})
        @credit_card_repository = overrides.fetch(:credit_card_repository) { CreditCardRepository.new }
        @infra_service = overrides.fetch(:infra_service) { Infra::CreditCardService.new }
      end

      def validate_payment(payment_id, cpf, amount, card_attributes)
        @infra_service.validate_payment(cpf: cpf, amount: amount, card_attributes: card_attributes).tap do |response|
          return false unless response.error.nil?

          return @credit_card_repository.create(
            number: card_attributes['number'], status: payment_status(response), payment_id: payment_id
          )
        end
      end

      private

      def payment_status(response)
        return 'denied' unless response.valid_payment

        'paid'
      end
    end
  end
end
