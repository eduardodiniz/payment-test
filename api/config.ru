$LOAD_PATH << File.expand_path(File.join(File.dirname(__FILE__)))

require File.expand_path '../apps/web/sinatra_application.rb', __FILE__
require 'dotenv/load'
require 'hanami/model'
require 'hanami/model/sql'

Dir["#{ __dir__ }/domain/**/*.rb"].each { |file| require file }
Dir["#{ __dir__ }/infra/**/*.rb"].each { |file| require file }
Dir["#{ __dir__ }/use_case/**/*.rb"].each { |file| require file }

Hanami::Model.configure do
  adapter :sql, ENV['DATABASE_URL']
end.load!

run Rack::URLMap.new({
  '/api' => SinatraApplication
})
