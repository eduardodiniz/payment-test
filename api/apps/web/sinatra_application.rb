require 'sinatra/base'
require 'sinatra/cross_origin'
require 'json'
require 'use_case/payment/create_payment'

class SinatraApplication < Sinatra::Base
  set :bind, '0.0.0.0'

  configure do
    enable :cross_origin
  end

  before do
    response.headers['Access-Control-Allow-Origin'] = '*'
  end

  get '/' do
    'Hello world'
  end

  post '/payment' do
    payload = JSON.parse(request.body.read)

    UseCase::Payment::CreatePayment.new.create(payload).tap do |response|
      return response.number if payload['payment']['type'] == 'boleto'

      return JSON.generate({'payment_status' => response.status})
    end
  end

  options "*" do
    response.headers["Allow"] = "GET, POST, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
    response.headers["Access-Control-Allow-Origin"] = "*"
    200
  end
end
