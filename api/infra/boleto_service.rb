require 'ostruct'

module Infra
  class BoletoService
    def initialize(overrides = {})
      @external_service = overrides.fetch(:external_service) { FakeService.new }
    end

    def generate_boleto(cpf:, amount:)
      response(number: @external_service.generate_boleto(cpf, amount))

    rescue Exception => error
      return response(error: error.message)
    end

    private

    def response(attributes)
      OpenStruct.new(number: attributes[:number], error: attributes[:error])
    end
  end
end

class FakeService
  def generate_boleto(cpf, amount)
    fail(Exception.new('Error: blablabla')) if cpf == '11111111111'

    return '0000111122000000033'
  end
end
