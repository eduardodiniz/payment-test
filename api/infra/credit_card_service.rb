require 'ostruct'

module Infra
  class CreditCardService
    def initialize(overrides = {})
      @external_service = overrides.fetch(:external_service) { FakeService.new }
    end

    def validate_payment(cpf:, amount:, card_attributes:)
      response(valid_payment: @external_service.validate(cpf, amount, card_attributes))

    rescue Exception => error
      return response(error: error.message)
    end

    private

    def response(attributes)
      OpenStruct.new(valid_payment: attributes[:valid_payment], error: attributes[:error])
    end
  end
end

class FakeService
  def validate(cpf, amount, card_attributes)
    fail(Exception.new('Error: blablabla')) if cpf == '11111111111'

    return false if cpf == '00000000000'

    true
  end
end
