require 'domain/payment/funding_instrument_gateway'
require 'domain/payment/payment_repository'
require 'ostruct'

module UseCase
  module Payment
    class CreatePayment
      def initialize(overrides = {})
        @payment_repository = overrides.fetch(:payment_repository) { PaymentRepository.new }
        @funding_instrument_gateway = overrides.fetch(:funding_instrument_gateway) do
          Domain::Payment::FundingInstrumentGateway.new
        end
      end

      def create(attributes)
        @payment_repository.create(
          type: attributes['payment']['type'],
          amount: attributes['payment']['amount'],
          client_id: attributes['client']['id'],
          buyer_cpf: attributes['buyer']['cpf'],
          buyer_name: attributes['buyer']['name'],
          buyer_email: attributes['buyer']['email']
        ).tap do |payment|
          return response(payment, attributes)
        end
      end

      private

      def create_funding_instrument(payment, attributes)
        @funding_instrument_gateway.create(
          payment: payment,
          credit_card_attributes: attributes['payment']['card']
        ).tap do |funding_response|
          fail(Exception.new("Payment Error")) unless funding_response
        end
      end

      def response(payment, attributes)
        create_funding_instrument(payment, attributes).tap do |funding_instrument|
          return OpenStruct.new(id: payment.id, number: funding_instrument.number, status: funding_instrument.status)
        end
      end
    end
  end
end
