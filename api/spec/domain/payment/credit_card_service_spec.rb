require 'domain/payment/credit_card_service'

describe Domain::Payment::CreditCardService do
  context 'when creating credit card payment' do
    it 'returns the persisted payment' do
      payment_id = 1
      cpf = '112131121212'
      amount = 240
      card_attributes = {'number' => '12312323212', cvv: 123}
      infra_service = instance_double(Infra::CreditCardService)
      credit_card_repository = instance_double(CreditCardRepository)
      credit_card_payment = double('CCPayment')
      allow(infra_service).to receive(:validate_payment).with(
        cpf: cpf, amount: amount, card_attributes: card_attributes
      ).and_return(double('Response', valid_payment: false, error: nil))
      allow(credit_card_repository).to receive(:create).and_return(credit_card_payment)

      subject = described_class.new(
        infra_service: infra_service, credit_card_repository: credit_card_repository
      ).validate_payment(payment_id, cpf, amount, card_attributes)

      expect(subject).to eq credit_card_payment
    end

    it 'persists paid status when payment is authorized' do
      payment_id = 1
      cpf = '112131121212'
      amount = 240
      card_attributes = {'number' => '12312323212', cvv: 123}
      infra_service = instance_double(Infra::CreditCardService)
      credit_card_repository = instance_spy(CreditCardRepository)
      infra_response = double('InfraResponse', valid_payment: true, error: nil)
      allow(infra_service).to receive(:validate_payment).and_return(infra_response)

      subject = described_class.new(
        infra_service: infra_service, credit_card_repository: credit_card_repository
      ).validate_payment(payment_id, cpf, amount, card_attributes)

      expect(credit_card_repository).to have_received(:create).with(
        number: card_attributes['number'], status: 'paid', payment_id: payment_id
      )
    end

    it 'persists denied status when payment is not authorized' do
      payment_id = 1
      cpf = '112131121212'
      amount = 240
      card_attributes = {'number' => '12312323212', cvv: 123}
      infra_service = instance_double(Infra::CreditCardService)
      credit_card_repository = instance_spy(CreditCardRepository)
      infra_response = double('InfraResponse', valid_payment: false, error: nil)
      allow(infra_service).to receive(:validate_payment).and_return(infra_response)

      subject = described_class.new(
        infra_service: infra_service, credit_card_repository: credit_card_repository
      ).validate_payment(payment_id, cpf, amount, card_attributes)

      expect(credit_card_repository).to have_received(:create).with(
        number: card_attributes['number'], status: 'denied', payment_id: payment_id
      )
    end

    context 'when an error occurs within infra service' do
      it 'returns false' do
      payment_id = 1
      cpf = '112131121212'
      amount = 240
      card_attributes = {'number' => '12312323212', cvv: 123}
      infra_service = instance_double(Infra::CreditCardService)
      credit_card_repository = instance_double(CreditCardRepository)
      infra_response = double('Response', valid_payment: nil, error: 'An error Message')
      credit_card_payment = double('CCPayment')
      allow(infra_service).to receive(:validate_payment).with(
        cpf: cpf, amount: amount, card_attributes: card_attributes
      ).and_return(infra_response)

      subject = described_class.new(
        infra_service: infra_service, credit_card_repository: credit_card_repository
      ).validate_payment(payment_id, cpf, amount, card_attributes)

      expect(subject).to be_falsey
      end
    end
  end
end
