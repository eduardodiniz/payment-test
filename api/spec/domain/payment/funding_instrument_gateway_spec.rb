require 'domain/payment/funding_instrument_gateway'

describe Domain::Payment::FundingInstrumentGateway do
  context 'when persisting funding instrument' do
    context 'when payment is within credit card' do
      it 'delegates persistence to the credit card service' do
        boleto_service = double('BoletoService')
        credit_card_service = instance_double(Domain::Payment::CreditCardService)
        payment = double('Payment', id: 'x123', amount: 300, type: 'credit_card', buyer_cpf: '110000192918')
        card_attributes = {number: '0101020292', cvv: 100}
        service_response = double('Response')

        allow(credit_card_service).to receive(:validate_payment).with(
          payment.id, payment.buyer_cpf, payment.amount, card_attributes
        ).and_return(service_response)

        subject = described_class.new(
          boleto_service: boleto_service,
          credit_card_service: credit_card_service
        ).create(payment: payment, credit_card_attributes: card_attributes)

        expect(subject).to eq service_response
      end
    end

    context 'when payment is within boleto' do
      it 'delegates persistence to the boleto service' do
        credit_card_service = double('CreditCardService')
        boleto_service = instance_double(Domain::Payment::BoletoService)
        payment = double('Payment', id: 'x123', amount: 300, type: 'boleto', buyer_cpf: '110000192918')
        service_response = double('Response')

        allow(boleto_service).to receive(:generate_boleto).with(
          payment.id, payment.buyer_cpf, payment.amount
        ).and_return(service_response)

        subject = described_class.new(
          credit_card_service: credit_card_service,
          boleto_service: boleto_service
        ).create(payment: payment)

        expect(subject).to eq service_response
      end
    end
  end
end
