require 'domain/payment/boleto'

describe Boleto do
  context 'when paying a boleto' do
    it 'returns true when payment is valid' do
      boleto = described_class.new(number: '00091292019', status: 'pending', due_date: '3000-01-01')

      expect(boleto.liquidate_debt).to be_truthy
    end

    it 'raises execption if boleto is overdue' do
      boleto = described_class.new(number: '00091292019', status: 'pending', due_date: '1900-01-01')

      expect{boleto.liquidate_debt}.to raise_exception('Overdue Boleto')
    end
  end
end
