require 'domain/payment/boleto_service'

describe Domain::Payment::BoletoService do
  context 'when generating boleto' do
    it 'returns the persisted boleto' do
      cpf = '12123123232'
      amount = 10
      payment_id = 1
      infra_service = instance_double(Infra::BoletoService)
      boleto_repository = instance_double(BoletoRepository)
      boleto_response = double('BoletoResponse', number: '00011199090100', error: nil)
      boleto = double('Boleto')
      allow(infra_service).to receive(:generate_boleto).with(cpf: cpf, amount: amount).and_return(boleto_response)
      allow(boleto_repository).to receive(:create).with(
        number: boleto_response.number,
        due_date: Date.today.to_s,
        payment_id: payment_id
      ).and_return(boleto)

      subject = described_class.new(
        infra_service: infra_service, boleto_repository: boleto_repository
      ).generate_boleto(payment_id, cpf, amount)

      expect(subject).to eq boleto
    end

    context 'when an error occurs within infra service' do
      it 'returns false' do
      cpf = '12123123232'
      amount = 10
      payment_id = 1
      infra_service = instance_double(Infra::BoletoService)
      boleto_repository = instance_double(BoletoRepository)
      infra_response = double('InfraResponse', error: 'An error message')
      allow(infra_service).to receive(:generate_boleto).with(cpf: cpf, amount: amount).and_return(infra_response)

      subject = described_class.new(
        infra_service: infra_service, boleto_repository: boleto_repository
      ).generate_boleto(payment_id, cpf, amount)

      expect(subject).to be_falsey
      end
    end
  end
end
