require 'use_case/payment/create_payment'

describe UseCase::Payment::CreatePayment do
  context 'when creating payment' do
    it 'returns persisted payment information' do
      funding_instrument_gateway = instance_double(Domain::Payment::FundingInstrumentGateway)
      payment_repository = instance_double(PaymentRepository)
      payment = double(
        'Payment', id: '23', amount: request_attributes[:payment][:amount], buyer_cpf: request_attributes[:buyer][:cpf]
      )
      funding_instrument = double('Response', number: '122323', status: 'fake-status')

      allow(payment_repository).to receive(:create).with(
        type: request_attributes['payment']['type'],
        amount: request_attributes['payment'['amount'],
        client_id: request_attributes['client']['id'],
        buyer_cpf: request_attributes['buyer']['cpf'],
        buyer_name: request_attributes['buyer'['name'],
        buyer_email: request_attributes['buyer'['email']
      ).and_return(payment)
      allow(funding_instrument_gateway).to receive(:create).with(
        payment: payment, credit_card_attributes: request_attributes['payment']['card']
      ).and_return(funding_instrument)

      subject = described_class.new(
        payment_repository: payment_repository, funding_instrument_gateway: funding_instrument_gateway
      ).create(request_attributes)

      expect(subject.id).to eq payment.id
      expect(subject.number).to eq funding_instrument.number
      expect(subject.status).to eq funding_instrument.status
    end

    context 'when funding instrument creation fail' do
      it 'raises payment error' do
        funding_instrument_gateway = instance_double(Domain::Payment::FundingInstrumentGateway)
        payment_repository = instance_double(PaymentRepository)
        payment = double(
          'Payment', id: '23', amount: request_attributes[:payment][:amount], buyer_cpf: request_attributes[:buyer][:cpf]
        )
        funding_instrument = double('Response', number: '122323', status: 'fake-status')

        allow(payment_repository).to receive(:create).and_return(payment)
        allow(funding_instrument_gateway).to receive(:create).with(
          payment: payment, credit_card_attributes: request_attributes['payment']['card']
        ).and_raise(Exception.new('Fake Payment Error'))

        subject = described_class.new(
          payment_repository: payment_repository, funding_instrument_gateway: funding_instrument_gateway
        )

        expect{ subject.create(request_attributes) }.to raise_error('Fake Payment Error')
      end
    end
  end

  def request_attributes
    {
      'client' => { 'id' => 1 },
      'buyer' => { 'name' => 'FAke', 'email' => 'test@test.com', 'cpf' => '1232222' },
      'payment' => {
        'type' => 'fake-type',
        'amount' => 12002,
        'client_id' => 1000,
        'card' => { 'cvv' => 123, 'holder_name' => 'Fake N', 'number' => '1238292', 'expiration_date' => '2018-01-01' }
      }
    }
  end

end
