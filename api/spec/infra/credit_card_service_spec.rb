require 'infra/credit_card_service'

describe Infra::CreditCardService do
  context 'when generating payment' do
    it 'returns object with the result of the credit card payment validation' do
      cpf = '12312312312'
      amount = 100
      card_attributes = {number: 1111111111, cvv: 111}
      external_service = double('External Service')
      allow(external_service).to receive(:validate).with(cpf, amount, card_attributes).and_return(true)

      subject = described_class.new(
        external_service: external_service  
      ).validate_payment(cpf: cpf, amount: amount, card_attributes: card_attributes)


      expect(subject.valid_payment).to be_truthy
    end

    context 'when external service returns error' do
      it 'returns object with error message' do
        cpf = '12312312312'
        amount = 100
        card_attributes = {number: 1111111111, cvv: 111}
        external_service = double('External Service')
        error_message = 'Error: fake error message'
        allow(external_service).to receive(:validate).with(
          cpf, amount, card_attributes
        ).and_raise(Exception.new(error_message))

        subject = described_class.new(
          external_service: external_service  
        ).validate_payment(cpf: cpf, amount: amount, card_attributes: card_attributes)


        expect(subject.valid_payment).to be_nil
        expect(subject.error).to eq error_message
      end
    end
  end
end
