require 'infra/boleto_service'

describe Infra::BoletoService do
  context 'when generating payment' do
    it 'returns object with boleto number' do
      cpf = '12312312312'
      amount = 150
      external_service = double('External Service')
      number = '0001110000002222200'
      allow(external_service).to receive(:generate_boleto).with(cpf, amount).and_return(number)

      subject = described_class.new(
        external_service: external_service  
      ).generate_boleto(cpf: cpf, amount: amount)


      expect(subject.number).to eq number
    end

    context 'when external service returns error' do
      it 'returns object with error message' do
        cpf = '12312312312'
        amount = 150
        external_service = double('External Service')
        error_message = 'Error: fake error message'
        allow(external_service).to receive(:generate_boleto).with(cpf, amount).and_raise(Exception.new(error_message))

        subject = described_class.new(
          external_service: external_service  
        ).generate_boleto(cpf: cpf, amount: amount)


        expect(subject.number).to be_nil
        expect(subject.error).to eq error_message
      end
    end
  end
end
