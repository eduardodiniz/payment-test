$LOAD_PATH << File.expand_path(File.join(File.dirname(__FILE__), '..'))

require 'bundler/setup'
require 'hanami/setup'
require 'hanami/model'
require_relative '../lib/api'
require_relative '../apps/web/application'

Dir["#{ __dir__ }/../domain/**/*.rb"].each { |file| require file }
Dir["#{ __dir__ }/../infra/**/*.rb"].each { |file| require file }
Dir["#{ __dir__ }/../use_case/**/*.rb"].each { |file| require file }

Hanami.configure do
  mount Web::Application, at: '/'

  model do
    ##
    # Database adapter
    #
    # Available options:
    #
    #  * SQL adapter
    #    adapter :sql, 'sqlite://db/api_development.sqlite3'
    #    adapter :sql, 'postgresql://localhost/api_development'
    #    adapter :sql, 'mysql://localhost/api_development'
    #
    adapter :sql, ENV.fetch('DATABASE_URL')

    ##
    # Migrations
    #
    migrations 'db/migrations'
    schema     'db/schema.sql'
  end

  mailer do
    root 'lib/api/mailers'

    # See http://hanamirb.org/guides/mailers/delivery
    delivery :test
  end

  environment :development do
    # See: http://hanamirb.org/guides/projects/logging
    logger level: :debug
  end

  environment :production do
    logger level: :info, formatter: :json, filter: []

    mailer do
      delivery :smtp, address: ENV.fetch('SMTP_HOST'), port: ENV.fetch('SMTP_PORT')
    end
  end
end
